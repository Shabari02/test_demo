const express = require("express");
const fs = require("fs");
const path = require("path");

const app = express();


app.use(express.static(path.join(__dirname, "public")));


app.use(express.urlencoded({ extended: true }));


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
